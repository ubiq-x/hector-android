/*

TODO
    Auto-connect at start
    Read: source.android.com/setup/code-style#follow-field-naming-conventions

References
    Mobile dev
        Components: material.io/guidelines/components/sliders.html
        Devices
            material.io/devices
            dpi.lv
    Rotation angles, orientation, azimuth, etc.
            developer.android.com/guide/topics/sensors/sensors_position.html#sensors-pos-orient
        [1] www.ssaurel.com/blog/get-android-device-rotation-angles-with-accelerometer-and-geomagnetic-sensors
        [2] developer.android.com/reference/android/hardware/SensorManager.html#remapCoordinateSystem(float[], int, int, float[])
            www.anandmuralidhar.com/blog/android/simple-vr
            www.journal.deviantdev.com/android-compass-azimuth-calculating
    TCP client
        [3] stackoverflow.com/questions/22018305/execCmd-data-over-tcp-from-android-app
            www.youtube.com/watch?v=QVq2IGg5ZFM
            stackoverflow.com/questions/38162775/really-simple-tcp-client
            www.myandroidsolutions.com/2013/03/31/android-tcp-connection-enhanced/#.Wk42Z0uQyEI
            github.com/codepath/android_guides/wiki/Sending-and-Receiving-Data-with-Sockets
    Service
            www.vogella.com/tutorials/AndroidServices/article.html#exercise_bindlocalservice
    Singleton class
        [4] android.jlelse.eu/how-to-make-the-perfect-singleton-de6b951dfdb0
    Toolbar and Appbar
            developer.android.com/training/appbar/index.html
            guides.codepath.com/android/using-the-app-toolbar
            stackoverflow.com/questions/8614293/android-get-view-reference-to-a-menu-item
    Add modules (AAR) to project
        developer.android.com/studio/projects/android-library.html
    Video streaming
        Android
            MJpeg Viewer
            MJpeg View
                forums.xamarin.com/discussion/35430/view-mjpg-video-streaming-in-xamarin-android
                    bitbucket.org/neuralassembly/simplemjpegview/overview
            WebView
                github.com/andy123205/Webview
        Public Mjpeg cameras
            http://96.10.1.168/mjpg/1/video.mjpg?timestamp=1515313991327
        RPi Cam Web Interface
            www.sitepoint.com/streaming-a-raspberry-pi-camera-into-vr-with-javascript
        UV4L
            Install
                sudo apt-get update
                sudo apt-get install uv4l uv4l-server uv4l-uvc uv4l-server uv4l-webrtc uv4l-xmpp-bridge
                man uv4l --> EXAMPLES
            www.linux-projects.org/uv4l/tutorials/dual-cameras
            www.raspberrypi.org/forums/viewtopic.php?t=152939
        MJpegStreamer
            Install:  github.com/cncjs/cncjs/wiki/Setup-Guide:-Raspberry-Pi-|-MJPEG-Streamer-Install-&-Setup-&-FFMpeg-Recording
                sudo apt-get install libjpeg-dev cmake
                cd ~/tmp
                git clone https://github.com/jacksonliam/mjpg-streamer
                make
                sudo make install

                sudo cp -r /usr/local/share/mjpg-streamer/www /usr/local/share/mjpg-streamer/www-l
                sudo cp -r /usr/local/share/mjpg-streamer/www /usr/local/share/mjpg-streamer/www-r
                sudo echo '<html><body style="margin:0px; background-color: #000;"><center><img src="./?action=stream" style="width: 100%;" /></center></body></html>' >> /usr/local/share/mjpg-streamer/www-l/stream_zero.html
                sudo cp /usr/local/share/mjpg-streamer/www-l/stream_zero.html /usr/local/share/mjpg-streamer/www-r

                /usr/local/bin/mjpg_streamer -i "input_uvc.so -r 640x480 -d /dev/video0 -f 30 -q 80" -o "output_http.so -p 3301 -w /usr/local/share/mjpg-streamer/www-l"
                /usr/local/bin/mjpg_streamer -i "input_uvc.so -r 640x480 -d /dev/video1 -f 30 -q 80" -o "output_http.so -p 3302 -w /usr/local/share/mjpg-streamer/www-r"
            github.com/foosel/OctoPrint/wiki/MJPG-Streamer-configuration
            virtlab.wordpress.com/2013/08/23/raspberry-pi-performance-with-video-streaming
            wolfpaulus.com/embedded/raspberrypi_webcam
            github.com/perthcpe23/android-mjpeg-view
            bitbucket.org/neuralassembly/simplemjpegview
                bitbucket.org/neuralassembly/simplemjpegview/src/6f7977956109?at=master
            github.com/michogar/MjpegView
            stackoverflow.com/questions/24114576/android-mjpeg-stream
            iotalot.com/2016/05/28/video-streaming-using-raspberry-pi-3-and-usb-webcam
        gstreamer
            Install
                sudo apt-get update
                sudo apt-get install gstreamer1.0-tools gstreamer1.0-plugins-ugly
                gst-launch-1.0 -vv -e v4l2src device=/dev/video0 ! videoscale ! "video/x-raw,width=640,height=480,framerate=10/1" ! x264enc pass=qual quantizer=20  tune=zerolatency ! h264parse ! rtph264pay config-interval=5 pt=96 ! udpsink host=192.168.0.180 port=3301
            Packages for RPi
                www.raspberrypi.org/forums/viewtopic.php?f=28&t=109268
            wiki.oz9aec.net/index.php/Gstreamer_cheat_sheet
        VLC
            www.lewisroberts.com/2015/05/15/raspberry-pi-mjpeg-at-30fps
        Hawkeye
            Install:  github.com/ipartola/hawkeye
                sudo apt-get install build-essential debhelper libv4l-dev libjpeg8-dev libssl-dev git
                cd ~/tmp
                git clone https://github.com/ipartola/hawkeye.git
                cd hawkeye/
                make
                sudo make install

                mkdir -p ~/data/hawkeye
                touch ~/data/hawkeye/hawkeye.conf
                sudo mkdir -p /usr/local/share/hawkeye/www
                hawkeye -c ~/data/hawkeye/hawkeye.conf -H 192.168.0.180 -p 3301 -w /usr/local/share/hawkeye/www -D /dev/video0 -W 640 -G 480 -j 80

                hawkeye --daemon --config=/home/tomek/data/hawkeye/hawkeye.conf --host=192.168.0.180 --port=3301 --www=/usr/local/share/mjpg-streamer/www --log=/home/tomek/data/hawkeye/hawkeye.log --devices=/dev/video0 --width=640 --height=480 --quality=80 --format=mjpeg
            www.raspberrypi.org/forums/viewtopic.php?f=35&t=77521
        VR
            www.sitepoint.com/streaming-a-raspberry-pi-camera-into-vr-with-javascript
        Misc
            lsusb
                ID 1415:2000 Nam Tai E&E Products Ltd. or OmniVision Technologies, Inc. Sony Playstation Eye
            v4l2-ctl --list-formats
                ioctl: VIDIOC_ENUM_FMT
                    Index       : 0
                    Type        : Video Capture
                    Pixel Format: 'YUYV'
                    Name        : YUYV 4:2:2
    TOREAD
        developer.android.com/reference/android/support/annotation/IntDef.html

Tasks
    Set button color
        btn.setColorFilter(Color.argb(255, 100, 100, 100));
    Perform task in the background
        new Thread(
             new Runnable() {
                 @Override
                 public void run() {
                     gvrAudioEngine.preloadSoundFile(OBJECT_SOUND_FILE);
                     sourceId = gvrAudioEngine.createSoundObject(OBJECT_SOUND_FILE);
                     gvrAudioEngine.setSoundObjectPosition(sourceId, modelPosition[0], modelPosition[1], modelPosition[2]);
                     gvrAudioEngine.playSound(sourceId, true);
                     gvrAudioEngine.preloadSoundFile(SUCCESS_SOUND_FILE);
                 }}).start();

----------------------------------------------------------------------------------------------------

UVC
    en.wikipedia.org/wiki/List_of_USB_video_class_devices
    www.uvccompatiblewebcams.com
UVC + H.264 on-board
    github.com/matthiasbock/gstreamer-phone/wiki/GStreamer-support-for-the-UVC-for-H.264-encoding-cameras
    en.wikipedia.org/wiki/List_of_cameras_with_onboard_video_compression
    smile.amazon.com/dp/B006JH8T3S

--------------------------------------------------------------------------------------------------*/


package net.tomek.hector;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;


// =================================================================================================
public class MainActivity extends AppCompatActivity implements ServiceConnection {

    private HectorService mSrvHector = null;

    private Button    mBtnActLV;
    private MenuItem  mMenuItemConn;
    private ImageView mImgConn;

    private boolean mIsConn = false;


    // ---------------------------------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // UI:
        setContentView(R.layout.activity_main);

        mBtnActLV = findViewById(R.id.btn_act_lv);
        mImgConn  = findViewById(R.id.img_conn);

        setSupportActionBar((Toolbar) findViewById(R.id.tbar));
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    protected void onPause() {
        super.onPause();

        unbindService(this);
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    protected void onResume() {
        super.onResume();

        Intent intent = new Intent(this, HectorService.class);
        bindService(intent, this, Context.BIND_AUTO_CREATE);
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        mMenuItemConn = menu.findItem(R.id.action_conn);
        return true;
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_conn:
                // ...
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    public void onServiceConnected(ComponentName name, IBinder binder) {
        HectorService.HectorBinder b = (HectorService.HectorBinder) binder;
        mSrvHector = b.getService();

        // mMenuItemConn.setIcon(R.drawable.robot_on);
        mImgConn.setVisibility(View.VISIBLE);
        mBtnActLV.setEnabled(true);
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    public void onServiceDisconnected(ComponentName name) {
        mSrvHector = null;

        //mMenuItemConn.setIcon(R.drawable.robot_off);
        mImgConn.setVisibility(View.INVISIBLE);
        mBtnActLV.setEnabled(false);
    }

    // ---------------------------------------------------------------------------------------------
    public void startActLV(View view) {
        startActivity(new Intent(this, LiveViewActivity.class));
    }
}
