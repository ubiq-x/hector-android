/*

TODO
    Measure ping over several sequential requests
        The better ping the more requests should be averaged over
    Keep a history of several most recent pings and average over them

--------------------------------------------------------------------------------------------------*/


package net.tomek.hector;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Timer;
import java.util.TimerTask;


// =================================================================================================
/**
 * Handles transport layer routines.
 *
 * Connection speed is estimated by pinging the host.
 */
public class Network implements Runnable {

    public  static final int  PING_TIMEOUT  = 2500;  // [ms]
    public  static final long PING_PERIOD   = 5000;  // [ms]; the time to wait before another ping
    public  static final long PING_PING_ERR =   -1;  // [ms]; lack of connectivity value


    private final Timer mTimer = new Timer();

    private String mHost = null;

    private int  mTimeout = PING_TIMEOUT;   // [ms]
    private long mPeriod  = PING_PERIOD;    // [ms]; the time to wait before another ping
    private long mPing    = PING_PING_ERR;  // [ms]


    // ---------------------------------------------------------------------------------------------
    public Network(String host) {
        mHost = host;
    }

    // ---------------------------------------------------------------------------------------------
    public Network(String host, int timeout, long period) {
        mHost    = host;
        mPeriod  = period;
        mTimeout = timeout;
    }

    // ---------------------------------------------------------------------------------------------
    public long getPing() { return mPing; }

    // ---------------------------------------------------------------------------------------------
    /**
     * Measures the speed of the connection with the host. Based on that speed it updates
     * parameters of future speed measurement.
     */
    public void ping() {
        boolean isReachable = false;
        long t0 = System.currentTimeMillis();

        try {
            isReachable = InetAddress.getByName(mHost).isReachable(mTimeout);
        }
        catch (IOException e) {
            mPing = PING_PING_ERR;
            e.printStackTrace();
        }

        long t1 = System.currentTimeMillis();

        mPing = (isReachable ? t1 - t0 : PING_PING_ERR);
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    public void run() {
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                ping();
            }
        }, 0, mPeriod);
    }
}
