/*

TODO
    ...

--------------------------------------------------------------------------------------------------*/


package net.tomek.hector;

import java.io.Serializable;
import java.util.Locale;


// =================================================================================================
/**
 * Handles all robot-related routines.
 *
 * A singleton class that is thread-safe, serialization-safe, and otherwise air-tight as described
 * in [4].
 */
class Hector implements Serializable {

    private static final int    NET_PING_TIMEOUT = 2500;  // [ms]
    private static final int    NET_PING_PERIOD  = 5000;  // [ms]; the time to wait between measuring network speed

    private static final long   THREAD_JOIN_TIME = 250;  // [ms]; the amount of time to wait for a thread to join

    public  static final String HOST = "192.168.0.182";
    public  static final int    PORT = 3333;


    private static volatile Hector sSelf = null;

    /*
    private HectorService mSrv        = null;
    private boolean       mIsSrvBound = false;
    */

    private Network mNet  = null;
    private Thread  mNetT = null;

    private TcpClient mClient  = null;  // this being null signifies no connection
    private Thread    mClientT = null;


    // ---------------------------------------------------------------------------------------------
    private Hector() {
        if (sSelf != null) {  // counter the intrusion of the Reflection API
            throw new RuntimeException("Use getInstance() method to get the single instance of the " + getClass().getName() + " class.");
        }

        //mSrv.start();
        /*
        Intent intent = new Intent(this, HectorService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        */
    }

    // ---------------------------------------------------------------------------------------------
    public static Hector getInstance() {
        if (sSelf == null) {
            synchronized (Hector.class) {  // synchronizing here prevents locking overhead and unnecessary post-initialization synchronization
                sSelf = new Hector();
            }
        }

        return sSelf;
    }

    // ---------------------------------------------------------------------------------------------
    public void connect() {
        if (mClient != null) return;

        mNet  = new Network(HOST, NET_PING_TIMEOUT, NET_PING_PERIOD);
        mNetT = new Thread(mNet);
        mNetT.start();

        mClient  = new TcpClient(HOST, PORT);
        mClientT = new Thread(mClient);
        mClientT.start();
    }

    // ---------------------------------------------------------------------------------------------
    public void disconnect() {
        if (mClient != null) {
            mClient.disconnect();
            try {
                mClientT.join(THREAD_JOIN_TIME);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            mClient = null;
            mClientT = null;
        }

        if (mNet != null) {
            try {
                mNetT.join(THREAD_JOIN_TIME);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            mNet  = null;
            mNetT = null;
        }
    }

    // ---------------------------------------------------------------------------------------------
    public float[] cmdCamGetCor() {
        mClient.exec(TcpClient.CMD_CAM_GET_SERVO_INF, true);
        return new float[] {0.0f, 0.0f, 0.0f};
    }

    // ---------------------------------------------------------------------------------------------
    public void cmdCamGoCenter() {
        mClient.exec(TcpClient.CMD_CAM_GO_CENTER, false);
    }

    // ---------------------------------------------------------------------------------------------
    public void cmdCamGoTo(float a, float r) {
        mClient.exec(String.format(Locale.US, "%s%s%6.4f%s%6.4f", TcpClient.CMD_CAM_GO_TO, TcpClient.DELIM, a, TcpClient.DELIM, r), false);
    }

    // ---------------------------------------------------------------------------------------------
    // public Network getNetwork() { return mSrv.getNetwork(); }
    public Network getNetwork() { return mNet; }

    // ---------------------------------------------------------------------------------------------
    /**
     * Makes the class serialization-safe.
     */
    protected Hector readResolve() {
        return getInstance();
    }

    // ---------------------------------------------------------------------------------------------
    /*
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            HectorBinder binder = (HectorBinder) service;
            mSrv = binder.getService();
            mIsSrvBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mIsSrvBound = false;
        }
    };
    */
}
