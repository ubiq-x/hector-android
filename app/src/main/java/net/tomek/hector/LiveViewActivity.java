/*

TODO
    Try the cardboard viewer on the 5x to see if pixels are visible like on the S5
    Display the white vertical line in the middle
    Cardboard and YouTube display the video as if it was warped by a glass dome -- try doing that
    Auto-center at start (after a short delay)
        Display a countdown (e.g., 3 seconds) and a request to face the desired direction
    Resize the video to account for the screen density (e.g., Samsung S5 is different than Nexus 5x)

Video streams
    http://192.168.0.180:3301/stream_simple.html
    http://192.168.0.180:3301/stream_zero.html
    http://192.168.0.180:3302/stream_zero.html
    http://192.168.0.180:3301/stream_lr.html

--------------------------------------------------------------------------------------------------*/


package net.tomek.hector;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Locale;


// =================================================================================================
/**
 * The last orientation to the robot is saved. A new orientation is send only if it is different
 * enough from the one sent most recently. That minimizes network traffic.
 *
 * Connection speed is measured via ping whenever the user centers the robot. That allows this
 * class to tailor the frequency of phone-robot orientation synchroniation messages to the capacity
 * of the available transport layer.
 */
public class LiveViewActivity extends AppCompatActivity
        implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener, SensorEventListener, ServiceConnection {

    // public  static final double R2A = 57.2957795131;  // radians to angle (i.e., 180 / PI)

    private static final float  ANGLE_DELTA_MIN = (float) (Math.PI / 90) / 2;
        // minimum change in angle for an orientation update to be sent to the robot

    private static final float  BTN_CENTER_ALPHA = 0.20f;  // transparency of the "Center" button after the view has been centered

    private static final String URI_EYE[] = new String[] {
            "http://192.168.0.180:3301/stream_zero.html",
            "http://192.168.0.180:3302/stream_zero.html"
    };
    private static final String URI_VID   = "http://192.168.0.180:3301/stream_lr.html";

    public  static final int    AXIS_A = 0;  // principal axis: azimuth
    public  static final int    AXIS_R = 1;  // principal axis: roll
    public  static final int    AXIS_P = 2;  // principal axis: pitch

    public  static final float  AXIS_A_MIN = (float) -Math.PI / 2;
    public  static final float  AXIS_A_MAX = (float)  Math.PI / 2;
    public  static final float  AXIS_R_MIN = (float) -Math.PI / 2;
    public  static final float  AXIS_R_MAX = (float)  Math.PI / 2;
    public  static final float  AXIS_P_MIN = (float) -Math.PI / 2;
    public  static final float  AXIS_P_MAX = (float)  Math.PI / 2;


    private HectorService mSrvHector = null;

    private View          mDecorView;
    private ImageButton   mBtnCenter;
    private ImageButton   mBtnRotErr;
    private ProgressBar[] mProgBar = new ProgressBar[3];
    private TextView      mTVDebug;
    // private WebView[]     mWVEye = new WebView[2];
    private WebView       mWVVid;

    private SensorManager mSensMan;
    private Sensor        mSensAcc = null;  // acceleration
    private Sensor        mSensGrv = null;  // gravitational field
    private Sensor        mSensMag = null;  // magnetic field
    private Sensor        mSensRV  = null;  // rotation vector
    private Sensor        mSensGRV = null;  // game rotation vector

    private GestureDetectorCompat mGesDet;

    private final float[] mAccVal     = new float[3];
    private final float[] mMagVal     = new float[3];
    private final float[] mRotMat     = new float[9];  // rotation matrix R
    private final float[] mRotAng     = new float[3];  // orientation angles [rad]
    private final float[] mRotAngHost = new float[3];  // ^ (the current orientation of the host; updated only when needed)
    private final float[] mRotAndCor  = new float[3];  // center correction [rad]

    private boolean mIsRotOk  = false;  // is the phone's orientation within bounds?
    private boolean mIsCenter = false;  // has the phone been centered
    private boolean mIsDebug  = false;  // has the debug mode been engaged


    // ---------------------------------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Hector:
        // System.arraycopy(sHector.cmdCamGetCor(), 0, mRotAndCor, 0, mRotAndCor.length);
        System.arraycopy(new float[] {0.0f, -480.0f, 0.0f}, 0, mRotAndCor, 0, 3);

        // Sensors:
        mSensMan = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if (mSensMan != null) {
            // mSensAcc = mSensMan.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            // mSensGrv = mSensMan.getDefaultSensor(Sensor.TYPE_GRAVITY);
            // mSensMag = mSensMan.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
            // mSensRV  = mSensMan.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
            mSensGRV = mSensMan.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR);
        }

        // Gestures:
        mGesDet = new GestureDetectorCompat(this,this);
        mGesDet.setOnDoubleTapListener(this);

        // UI:
        setContentView(R.layout.activity_lv);
        mDecorView = getWindow().getDecorView();

        mBtnCenter = findViewById(R.id.btn_center);
        mBtnRotErr = findViewById(R.id.btn_rot_err);
        /*
        mWVEye[0]  = findViewById(R.id.webview_left);
        mWVEye[1]  = findViewById(R.id.webview_right);
        */
        mWVVid     = findViewById(R.id.webview_vid);

        mBtnCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                center(view);
            }
        });

        /*
        mWVEye[0].getLayoutParams().height = Math.round(pxFromDp(getApplicationContext(), 720));
        mWVEye[0].getLayoutParams().width  = Math.round(pxFromDp(getApplicationContext(), 540));
        mWVEye[1].getLayoutParams().height = Math.round(pxFromDp(getApplicationContext(), 720));
        mWVEye[1].getLayoutParams().width  = Math.round(pxFromDp(getApplicationContext(), 540));

        mWVEye[0].getSettings().setUseWideViewPort(true);
        mWVEye[0].setInitialScale(1);
        mWVEye[1].getSettings().setUseWideViewPort(true);
        mWVEye[1].setInitialScale(1);
        */

        mWVVid.getSettings().setUseWideViewPort(true);
        mWVVid.setInitialScale(1);

        /*
        mVidL.setMode(MjpegView.MODE_ORIGINAL);
        mVidL.setAdjustHeight(true);
        mVidL.setUrl("http://192.168.0.180:3301/?action=stream");
        mVidL.setUrl("http://96.10.1.168/mjpg/1/video.mjpg?timestamp=1515313991327");
        mVidL.startStream();
        */

        mProgBar[0] = findViewById(R.id.seek_bar_01);
        mProgBar[1] = findViewById(R.id.seek_bar_02);
        mProgBar[2] = findViewById(R.id.seek_bar_03);

        mProgBar[0].setEnabled(false);
        mProgBar[1].setEnabled(false);
        mProgBar[2].setEnabled(false);

        mTVDebug = findViewById(R.id.tv_debug);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            mDecorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE          |
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN      |
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION        |
                View.SYSTEM_UI_FLAG_FULLSCREEN             |
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            );
        }
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    protected void onPause() {
        super.onPause();

        mSensMan.unregisterListener(this);

        mBtnRotErr.setVisibility(View.INVISIBLE);

        mProgBar[0].setVisibility(View.INVISIBLE);
        mProgBar[1].setVisibility(View.INVISIBLE);
        mProgBar[2].setVisibility(View.INVISIBLE);

        mTVDebug.setVisibility(View.INVISIBLE);

        mIsCenter = false;

        unbindService(this);

        /*
        mWVEye[0].stopLoading();
        mWVEye[1].stopLoading();
        */
        mWVVid.stopLoading();

        //mVidL.stopStream();

        finish();  // return to the main activity
        // TODO:
        // Don't finish the activity, but instead allow the user to center again. The problem
        // is that right now when returning to the app, the app bar is shown. Hide it in
        // 'onResume()' and we're in business.
    }


    // ---------------------------------------------------------------------------------------------
    @Override
    protected void onResume() {
        super.onResume();

        Intent intent = new Intent(this, HectorService.class);
        bindService(intent, this, Context.BIND_AUTO_CREATE);
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}

    // ---------------------------------------------------------------------------------------------
    @Override
    public void onSensorChanged(SensorEvent event) {
        if(mDecorView.getDisplay() == null) return;

        float[] evtVal = new float[3];

        // (1) Adjust the sensor data accounting for device rotation:
        switch (mDecorView.getDisplay().getRotation()) {
            case Surface.ROTATION_0:
                evtVal[0] = event.values[0];
                evtVal[1] = event.values[1];
                evtVal[2] = event.values[2];
                break;
            case Surface.ROTATION_90:  // always this one because portrait orientation is enforced
                evtVal[0] = -event.values[1];
                evtVal[1] = event.values[0];
                evtVal[2] = event.values[2];
                break;
            case Surface.ROTATION_180:
                evtVal[0] = -event.values[0];
                evtVal[1] = -event.values[1];
                evtVal[2] = event.values[2];
                break;
            case Surface.ROTATION_270:
                evtVal[0] = event.values[1];
                evtVal[1] = -event.values[0];
                evtVal[2] = event.values[2];
                break;
        }

        // (2) Store the rotation-adjusted sensor data:
        switch (event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
            case Sensor.TYPE_GRAVITY:
                System.arraycopy(evtVal, 0, mAccVal, 0, mAccVal.length);
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                System.arraycopy(evtVal, 0, mMagVal,  0, mMagVal.length );
                break;
            case Sensor.TYPE_ROTATION_VECTOR:
            case Sensor.TYPE_GAME_ROTATION_VECTOR:
                SensorManager.getRotationMatrixFromVector(mRotMat, evtVal);
                break;
        }

        update();
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mGesDet.onTouchEvent(event);

        /*
        switch(event.getAction()) {
            case (MotionEvent.ACTION_DOWN):
                return true;
            case (MotionEvent.ACTION_MOVE):
                return true;
            case (MotionEvent.ACTION_UP):
                return true;
            case (MotionEvent.ACTION_CANCEL):
                return true;
            case (MotionEvent.ACTION_OUTSIDE):
                return true;
            default:
                return super.onTouchEvent(event);
        }
        */

        return super.onTouchEvent(event);
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    public boolean onDown(MotionEvent e) {
        // Toast.makeText(getApplicationContext(), "onDown: " + e.toString(), Toast.LENGTH_SHORT).show();
        return true;
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velX, float velY) {
        // Toast.makeText(getApplicationContext(), "onFling: " + e1.toString() + e2.toString(), Toast.LENGTH_SHORT).show();

        if (e1.getY() > e2.getY()) {
            if (mIsDebug) mTVDebug.setVisibility(View.INVISIBLE);
            else mTVDebug.setVisibility(View.VISIBLE);
            mIsDebug = !mIsDebug;
        }

        return true;
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    public void onLongPress(MotionEvent e) {
        // Toast.makeText(getApplicationContext(), "onLongPress: " + e.toString(), Toast.LENGTH_SHORT).show();
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distX, float distY) {
        // Toast.makeText(getApplicationContext(), "onScroll: " + e1.toString() + e2.toString(), Toast.LENGTH_SHORT).show();
        return true;
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    public void onShowPress(MotionEvent e) {
        // Toast.makeText(getApplicationContext(), "onShowPress: " + e.toString(), Toast.LENGTH_SHORT).show();
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        // Toast.makeText(getApplicationContext(), "onSingleTapUp: " + e.toString(), Toast.LENGTH_SHORT).show();
        return true;
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    public boolean onDoubleTap(MotionEvent e) {
        // Toast.makeText(getApplicationContext(), "onDoubleTap: " + e.toString(), Toast.LENGTH_SHORT).show();
        return true;
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        // Toast.makeText(getApplicationContext(), "onDoubleTapEvent: " + e.toString(), Toast.LENGTH_SHORT).show();
        return true;
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        // Toast.makeText(getApplicationContext(), "onSingleTapConfirmed: " + e.toString(), Toast.LENGTH_SHORT).show();
        return true;
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    public void onServiceConnected(ComponentName name, IBinder binder) {
        HectorService.HectorBinder b = (HectorService.HectorBinder) binder;
        mSrvHector = b.getService();
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    public void onServiceDisconnected(ComponentName name) {
        mSrvHector = null;
    }

    // ---------------------------------------------------------------------------------------------
    public void center(View view) {
        registerSensors((int) mSrvHector.getNetwork().getPing() * 1000);

        mBtnCenter.setAlpha(BTN_CENTER_ALPHA);

        /*
        mWVEye[0].setVisibility(View.VISIBLE);
        mWVEye[1].setVisibility(View.VISIBLE);

        mWVEye[0].loadUrl(URI_EYE[0]);
        mWVEye[1].loadUrl(URI_EYE[1]);
        */
        mWVVid.setVisibility(View.VISIBLE);
        mWVVid.loadUrl(URI_VID);

        if (mIsDebug) {
            mProgBar[0].setVisibility(View.VISIBLE);
            mProgBar[1].setVisibility(View.VISIBLE);
            mProgBar[2].setVisibility(View.VISIBLE);

            mTVDebug.setVisibility(View.VISIBLE);
        }

        // sHector.cmdCamGoCenter();
        mSrvHector.cmdCamGoCenter();

        mIsCenter = true;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * Can accept ping as delay so it should account for it being negative indicating ping error.
     */
    public void registerSensors(int delay) {
        mSensMan.unregisterListener(this);

        if (delay < 0) {
            delay = 10000;  // 10ms
                // SENSOR_DELAY_FASTEST        0 us delay
                // SENSOR_DELAY_GAME      20,000 us delay
                // SENSOR_DELAY_UI        60,000 us delay
                // SENSOR_DELAY_NORMAL   200,000 us delay
        }

        if (mSensAcc != null) { mSensMan.registerListener(this, mSensAcc, delay); }
        if (mSensGrv != null) { mSensMan.registerListener(this, mSensGrv, delay); }
        if (mSensMag != null) { mSensMan.registerListener(this, mSensMag, delay); }
        if (mSensRV  != null) { mSensMan.registerListener(this, mSensRV,  delay); }
        if (mSensGRV != null) { mSensMan.registerListener(this, mSensGRV, delay); }
    }

    // ---------------------------------------------------------------------------------------------
    public void update() {
        updateOrientation();

        float angA = -mRotAng[AXIS_A];  // reverse direction to match the robot
        float angR = -mRotAng[AXIS_R];
        float angP =  mRotAng[AXIS_P];

        boolean isAngDeltaOk = (Math.abs(mRotAng[AXIS_A] - mRotAngHost[AXIS_A]) >= ANGLE_DELTA_MIN || Math.abs(mRotAng[AXIS_R] - mRotAngHost[AXIS_R]) >= ANGLE_DELTA_MIN);
            // is the orientation change large enough (compared to the last one sent) to warrant a robot orientation update

        if (mIsCenter && mIsRotOk && isAngDeltaOk) {
            mSrvHector.cmdCamGoTo(angA, angR);
            System.arraycopy(mRotAng, 0, mRotAngHost, 0, mRotAng.length);  // remember the orientation sent to the robot
        }

        if (mIsDebug) {
            mProgBar[0].setProgress((int) Math.round(Math.toDegrees(angA)) + 180);
            mProgBar[1].setProgress((int) Math.round(Math.toDegrees(angR)) + 180);
            mProgBar[2].setProgress((int) Math.round(Math.toDegrees(angP)) + 180);

            mTVDebug.setText(String.format(Locale.US, "(%.2f, %.2f, %.2f)  [%d ms]", angA, angR, angP, mSrvHector.getNetwork().getPing()));
        }
    }

    // ---------------------------------------------------------------------------------------------
    public void updateOrientation() {
        // (1) Update the angles:
        float[] mRotMat_tmp = new float[9];

        if ((mSensAcc != null || mSensGrv != null) && mSensMag != null) {  // rotation vector sensor updates rotation matrix in 'onSensorChanged()'
            SensorManager.getRotationMatrix(mRotMat, null, mAccVal, mMagVal);
        }
        SensorManager.remapCoordinateSystem(mRotMat, SensorManager.AXIS_X, SensorManager.AXIS_Z, mRotMat_tmp);  // [1,2] use the camera axis (VR or AR)
        SensorManager.getOrientation(mRotMat_tmp, mRotAng);
        mRotAng[1] += Math.PI / 4;

        // (2) Check if the phone's orientation is within defined bound:
        mRotAng[AXIS_A] = Math.min(Math.max(mRotAng[AXIS_A], AXIS_A_MIN), AXIS_A_MAX);

        float angA =  mRotAng[AXIS_A];
        float angR = -mRotAng[AXIS_R];  // reverse direction so that looking up yields positive values
        float angP =  mRotAng[AXIS_P];

        // (2.1) Orientation not Ok:
        if (angR < AXIS_R_MIN || angR > AXIS_R_MAX || angP < AXIS_P_MIN || angP > AXIS_P_MAX) {
            if (mIsRotOk) {  // show the error not being displayed yet
                mIsRotOk = false;

                /*
                mWVEye[0].setVisibility(View.INVISIBLE);
                mWVEye[1].setVisibility(View.INVISIBLE);
                */
                mWVVid.setVisibility(View.INVISIBLE);

                mBtnCenter.setVisibility(View.INVISIBLE);

                mBtnRotErr.setRotation((float) -Math.toDegrees(angP));
                mBtnRotErr.setVisibility(View.VISIBLE);

                if (mIsDebug) {
                    mProgBar[0].setVisibility(View.INVISIBLE);
                    mProgBar[1].setVisibility(View.INVISIBLE);
                    mProgBar[2].setVisibility(View.INVISIBLE);
                }
            }
            else {  // the error is already being displayed -- update its angle
                mBtnRotErr.setRotation((float) -Math.toDegrees(angP));
            }
        }

        // (2.2) Orientation Ok:
        else {
            if (!mIsRotOk) {  // hide the error still being displayed
                mBtnRotErr.setVisibility(View.INVISIBLE);

                /*
                mWVEye[0].setVisibility(View.VISIBLE);
                mWVEye[1].setVisibility(View.VISIBLE);
                */
                mWVVid.setVisibility(View.VISIBLE);

                mBtnCenter.setVisibility(View.VISIBLE);

                if (mIsDebug) {
                    mProgBar[0].setVisibility(View.VISIBLE);
                    mProgBar[1].setVisibility(View.VISIBLE);
                    mProgBar[2].setVisibility(View.VISIBLE);
                }

                mIsRotOk = true;
            }
        }
    }
}
