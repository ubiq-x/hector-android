package net.tomek.hector;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.Locale;


// =================================================================================================
/**
 * Handles transport layer routines.
 *
 * Connection speed is estimated by pinging the host.
 */
public class HectorService extends Service {

    private static final int    NET_PING_TIMEOUT = 2500;  // [ms]
    private static final int    NET_PING_PERIOD  = 5000;  // [ms]; the time to wait between measuring network speed

    private static final long   THREAD_JOIN_TIME = 500;  // [ms]; the amount of time to wait for a thread to join

    public  static final String HOST = "192.168.0.182";
    public  static final int    PORT = 3333;


    private boolean mAllowRebind = false;
    private final IBinder mBinder = new HectorBinder();

    private Network mNet  = null;
    private Thread  mNetT = null;

    private TcpClient mClient  = null;  // this being null signifies no connection
    private Thread    mClientT = null;


    // ---------------------------------------------------------------------------------------------

    /**
     * TODO: In this app only one activity can bind to the service at a time. However, it may still
     *       be possible for the mNet and mClient objects to not be initialized when the second
     *       activity attempt to bind. Make this code synchronized to avoid problems.
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        if (mNet == null) {
            mNet  = new Network(HOST, NET_PING_TIMEOUT, NET_PING_PERIOD);
            mNetT = new Thread(mNet);
            mNetT.start();
        }

        if (mClient == null) {
            mClient  = new TcpClient(HOST, PORT);
            mClientT = new Thread(mClient);
            mClientT.start();
        }

        return mBinder;
    }

    // ---------------------------------------------------------------------------------------------
    @Override
    public void onRebind(Intent intent) {}

    // ---------------------------------------------------------------------------------------------
    @Override
    public boolean onUnbind(Intent intent) {
        if (mNet != null) {
            try {
                mNetT.join(THREAD_JOIN_TIME);
            }
            catch (InterruptedException e) { e.printStackTrace(); }

            mNet  = null;
            mNetT = null;
        }

        if (mClient != null) {
            mClient.disconnect();
            try {
                mClientT.join(THREAD_JOIN_TIME);
            }
            catch (InterruptedException e) { e.printStackTrace(); }

            mClient = null;
            mClientT = null;
        }

        return mAllowRebind;
    }

    // ---------------------------------------------------------------------------------------------
    public float[] cmdCamGetCor() {
        mClient.exec(TcpClient.CMD_CAM_GET_SERVO_INF, true);
        return new float[] {0.0f, -480.0f, 0.0f};
    }

    // ---------------------------------------------------------------------------------------------
    public void cmdCamGoCenter() { mClient.exec(TcpClient.CMD_CAM_GO_CENTER, false); }

    // ---------------------------------------------------------------------------------------------
    public void cmdCamGoTo(float a, float r) {
        mClient.exec(String.format(Locale.US, "%s%s%6.4f%s%6.4f", TcpClient.CMD_CAM_GO_TO, TcpClient.DELIM, a, TcpClient.DELIM, r), false);
    }

    // ---------------------------------------------------------------------------------------------
    public Network getNetwork() { return mNet; }

    // =============================================================================================
    public class HectorBinder extends Binder {
        HectorService getService() {
            return HectorService.this;
        }
    }

    /*
    public class LocalBinder extends Binder {
        LocalService getService() {
            // Return this instance of LocalService so clients can call public methods
            return LocalService.this;
        }
    }
    */
}
