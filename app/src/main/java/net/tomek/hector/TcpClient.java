package net.tomek.hector;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Locale;


// =================================================================================================
/**
 * This TCP client run in a separate thread as described in [3].
 */
public class TcpClient implements Runnable {

    private static final String RET_OK      = "0";
    private static final String RET_ERR     = "1";
    private static final String RET_CMD_ERR = "2";

    private static final int    MSG_IN_LEN  = 4;
    private static final int    MSG_OUT_LEN = 4;

    private static final String CMD_DISCONNECT = "disconnect";


    public  static final String DELIM = " ";  // command argument delimiter

    public  static final String CMD_CAM_GET_SERVO_INF = "cam-get-servo-inf";
    public  static final String CMD_CAM_GO_CENTER     = "cam-go-center";
    public  static final String CMD_CAM_GO_TO         = "cam-go-to";


    private String mHost = null;
    private int    mPort = 0;

    private Socket         mSocket = null;
    private PrintWriter    mOut    = null;
    private BufferedReader mIn     = null;


    // ---------------------------------------------------------------------------------------------
    public TcpClient(String host, int port) {
        mHost = host;
        mPort = port;
    }

    // ---------------------------------------------------------------------------------------------
    public void disconnect() {
        if (mSocket == null) return;

        exec(CMD_DISCONNECT, false);
        try {
            mOut.close();
            mSocket.close();
        }
        catch (IOException e) { e.printStackTrace(); }
        finally {
            mOut   = null;
            mSocket = null;
        }
    }

    // ---------------------------------------------------------------------------------------------
    public String exec(String cmd, boolean doRes) {
        if (mSocket == null) return null;

        try {
            String msg = String.format(Locale.US, "%" + MSG_OUT_LEN + "d%s", cmd.length(), cmd);

            Log.d("TcpClient", msg);

            mOut.println(msg);
            mOut.flush();

            if (!doRes) return null;

            char[] res01 = new char[MSG_IN_LEN];
            mIn.read(res01, 0, MSG_IN_LEN);
            int n = Integer.parseInt(String.valueOf(res01));
            char[] res02 = new char[n];
            mIn.read(res02, 0, n);

            Log.d("TcpClient", String.format(Locale.US, "%s|%s", String.valueOf(res01), String.valueOf(res02)));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // ---------------------------------------------------------------------------------------------
    /**
     * This method must open the mSocket because due to limitation in Android only a separate
     * thread can do that.  My attempts to move this code to the 'connect()' method for a more
     * clean code have proven unsuccessful.
     */
    @Override
    public void run() {
        if (mSocket != null) return;

        try {
            mSocket = new Socket(mHost, mPort);
            mOut    = new PrintWriter (new BufferedWriter(new OutputStreamWriter(mSocket.getOutputStream())), true);
            //mIn     = new StringReader(new BufferedReader(new InputStreamReader (mSocket.getInputStream ())));
            mIn     = new BufferedReader(new InputStreamReader(mSocket.getInputStream()));

            // mOut    = new PrintWriter(new OutputStreamWriter(mSocket.getOutputStream()));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
