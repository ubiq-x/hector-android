# Hector: Android
A prototype of an Android app for Hector the robot enabling teleoperation and telepresence.

## Introduction
This is a phone client that allows remote control of Hector the robot.  It is an Android app which connects to the robot through a dedicated [TCP server](https://gitlab.com/ubiq-x/hector).  When connected, the robot's head movement and the phone's orientation are synced, i.e., one controls the movement of the robot's head by rotating the phone.  Both pan and tilt are supported.

Hector has two cameras mounted on its head.  When they are streaming, the Android app displays the two feeds side-by-side for binocular viewing.  With proper phone mount, that grants stereoscopic vision and thus telepresence.  I've been using the Pocket 360, but any virtual reality phone mount will do.


## Dependencies
- Android 6.0 (Marshmallow; API 23)
- Samsung Galaxy 5S (well, not really; see below)
- The app connects to [Hector's](https://gitlab.com/ubiq-x/hector) TCP server

You can in fact use any Android phone, but if the resolution or screen density is different than that on the Samsung Galaxy 5S, you will need to make proper changes to the code.  This is a prototype, remember?  It's a terrible phone by the way and the reason I never want a Samsung phone again, but since I had it already I thought I might as well repurpose it.


## Setup
### Hardware
See [Hector](https://gitlab.com/ubiq-x/hector) repo for info.

### Software
Use [Android Studio](https://developer.android.com) to build and deploy the app.

Note: The [MJPEG View 1.0.4](https://gitlab.com/perthcpe23/android-mjpeg-view) isn't used at the moment.  I've been experimenting with it, but sadly never got it to work.  I left it there for now hoping that perhaps one day I will be more lucky.


### Videos
A few short videos showing the progress on the robot:
- [Hector 01: Android phone orientation test](https://www.youtube.com/watch?v=OKBewA5BHsY)
- [Hector 02: Teleoperation and telepresence](https://www.youtube.com/watch?v=wH5Nxwbf1Uo)
